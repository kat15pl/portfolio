var casper = require("casper").create(),
    screenshotUrls = [
        {
            name: 'KA',
            url: 'http://krzysztofadamczewski.pl/',
        },
        {
            name: 'PWN',
            url: 'http://ksiegarnia.pwn.pl/',
        },
        {
            name: 'PZWL',
            url: 'http://pzwl.pl/',
        },
        {
            name: 'EGMONT',
            url: 'http://egmont.pl/',
        },
        {
            name: 'Przedszkole',
            url: 'http://przedszkole222.pl/',
        },
        {
            name: 'LSI',
            url: 'http://lsisa.pl/',
        },
        {
            name: 'CR',
            url: 'http://creditroyal.pl/'
        },
        {
            name: 'AC',
            url: 'http://www.euro-sky.pl/'
        }
    ],
    screenshotNow = new Date(),
    viewports = [
        {
            width: 480,
            name: 'Mobile'
        },
        {
            width: 768,
            name: 'Tablet'
        },
        {
            width: 1400,
            name: 'Desktop'
        }
    ];

casper.start();
casper.each(screenshotUrls, function (casper, screenshotUrl) {
    var urlName = screenshotUrl.name,
        url = screenshotUrl.url;

    casper.each(viewports, function (casper, viewport) {
        var file = 'screenshots/' + urlName + viewport.name + '.png',
            name = 'Screenshot for ' + urlName + '(' + viewport.name + ')';
        this.then(function () {
            this.viewport(viewport.width, 1000);
        });
        this.thenOpen(url, function () {
            this.wait(5000);
        });
        this.then(function () {
            this.echo(name, 'info');
            this.capture(file);
        });
    });
});

casper.run();
