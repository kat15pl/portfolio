// including plugins
let gulp = require("gulp"),
    sass = require("gulp-sass"),
    watchSass = require("gulp-watch-sass");

// task
gulp.task("compile-sass", function () {
    gulp.src("./src/scss/**.{scss,css}")
        .pipe(sass({
            includePaths: ["node_modules"],
            outputStyle: 'compressed'
        }))
        .pipe(gulp.dest("./src/css"));
});

gulp.task("sass:watch", () => {
    gulp.watch([
        "./src/scss/**.{scss,css}"
    ], ["compile-sass"]);
    gulp.watch([
        "./src/scss/**/**.{scss,css}"
    ], ["compile-sass"]);
});