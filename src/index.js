import React from "react";
import ReactDOM from "react-dom";
import {Switch, Route, MemoryRouter} from "react-router-dom";
import "./css/bootstrap.css";
import "./css/global.css";
import MainSite from "./sites/Main";
import PortfolioSite from "./sites/PortfolioSite";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
    <MemoryRouter>
        <Switch>
            <Route path="/portfolio/:siteLang/:site/" component={PortfolioSite}/>
            <Route path="*" component={MainSite}/>
        </Switch>
    </MemoryRouter>, document.getElementById("root"));
registerServiceWorker();
