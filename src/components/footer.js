import React from "react";
import "./../css/footer.css";

export default class Footer extends React.Component {

    constructor(props) {
        super(props);
        this.state = require(`../data/${props.lang}/footer`);
    }

    componentWillReceiveProps(props) {
        if (props.lang) {
            this.setState(require(`../data/${props.lang}/footer`));
        }
    }

    render() {
        return (
            <footer className="sectionFooter">
                <div className="footer">
                    <div className="container">
                        <div className="row">
                            <div className="footer-col col-lg-12">
                                <h3>{this.state.header}</h3>
                                <p>{this.state.telephone}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
