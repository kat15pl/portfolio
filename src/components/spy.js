import React, {Component} from "react";
import ReactDOM from "react-dom";

export default class Spy extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            offset: props.offset || 0
        };
        this.handleScroll = this.handleScroll.bind(this);
        this.getProps = this.getProps.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            offset: props.offset || 0
        })
    }

    componentDidMount() {
        this.handleScroll();
        window.addEventListener("scroll", this.handleScroll);
    }

    handleScroll(event) {
        // ReactDOM.findDOMNode(this.menu).getBoundingClientRect().bottom,
        let menuHeight = 100,
            top = ReactDOM.findDOMNode(this.component).getBoundingClientRect().top,
            bottom = ReactDOM.findDOMNode(this.component).getBoundingClientRect().bottom,
            isVisible = (top + this.state.offset <= window.innerHeight - menuHeight) && !(bottom + this.state.offset <= window.innerHeight - menuHeight);
        this.setState({
            isVisible: isVisible
        });
    }

    render() {
        return React.Children.map(this.props.children, child => {
            return React.cloneElement(
                child, {
                    ref: component => this.component = component,
                    isVisible: this.state.isVisible
                }
            )
        })
    }
}
