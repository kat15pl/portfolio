import React from "react";
import axios from "axios";
import AnimateOnVisible from "./animateOnVisible";
import Input from "./input";
import Textarea from "./textarea";
import "./../css/contact.css";

export default class Contact extends React.Component {

    constructor(props) {
        super(props);
        let errors = {
                message: false,
                telephone: false,
                name: false,
                email: false
            },
            values = {
                message: "",
                telephone: "",
                name: "",
                email: ""
            },
            data = require(`../data/${props.lang}/contact`);
        this.state = ({
            values: values,
            error: errors,
            header: data.header,
            errorLabel: data.errorLabel,
            placeholder: data.placeholder,
            send: data.send
        });
        this.change = this.change.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.lang) {
            let data = require(`../data/${props.lang}/contact`);
            this.setState({
                header: data.header,
                errorLabel: data.errorLabel,
                placeholder: data.placeholder,
                send: data.send
            });
        }
    }

    change(key, value) {
        let errors = Object.assign({}, this.state.error),
            values = Object.assign({}, this.state.values);
        errors[key] = !value;
        values[key] = value;
        this.setState({
            values: values,
            error: errors
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let isOk = true,
            key,
            errors = Object.assign({}, this.state.error);
        for (key in this.state.values) {
            if (!this.state.values[key]) {
                errors[key] = true;
                this.setState({
                    error: errors
                });
                isOk = false;
            }
        }
        if (isOk) {
            let data = JSON.stringify(this.state.values);
            axios.post("/api/send", {
                data
            })
                .then(function (response) {
                    console.log(response);
                });
        }
    }

    render() {
        return (
            <section id="contact">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h2>
                                {this.state.header.split("").map((letter, index) => {
                                    let delay = `${index * 500}ms`;
                                    return (
                                        <AnimateOnVisible animateIn="fadeIn" delay={delay} once={true} key={index}>
                                            {letter}
                                        </AnimateOnVisible>
                                    )
                                })}
                            </h2>
                            <hr className="star-primary"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-8 col-lg-offset-2">
                            <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit}>
                                <div className="row control-group">
                                    <Input label={this.state.placeholder.name} placeholder={this.state.placeholder.name}
                                           id="name" error={this.state.error.name}
                                           errorLabel={this.state.errorLabel.name} onChange={this.change}/>
                                </div>
                                <div className="row control-group">
                                    <Input label={this.state.placeholder.email}
                                           placeholder={this.state.placeholder.email} id="email"
                                           error={this.state.error.email} errorLabel={this.state.errorLabel.email}
                                           onChange={this.change}/>
                                </div>
                                <div className="row control-group">
                                    <Input label={this.state.placeholder.telephone}
                                           placeholder={this.state.placeholder.telephone} id="telefon"
                                           error={this.state.error.telephone}
                                           errorLabel={this.state.errorLabel.telephone} onChange={this.change}/>
                                </div>
                                <div className="row control-group">
                                    <Textarea label={this.state.placeholder.message}
                                              placeholder={this.state.placeholder.message} id="message"
                                              error={this.state.error.message}
                                              errorLabel={this.state.errorLabel.message} onChange={this.change}/>
                                </div>
                                <br/>
                                <div id="success"></div>
                                <div className="row">
                                    <div className="form-group col-xs-12">
                                        <button type="submit"
                                                className="btn btn-success btn-lg">{this.state.send}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
