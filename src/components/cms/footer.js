import React from "react";
import "./../../css/cms/footer.css";

export default class Footer extends React.Component {

    render() {
        return (
            <footer id="footer">
                <div className="footer">
                    <div className="container">
                        <div className="row">
                            <div className="footer-col col-lg-12">
                                <p>(c) 2017 by Krzysztof Adamczewski</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
