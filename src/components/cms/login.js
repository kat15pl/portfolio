import React from "react";
import "./../../css/cms/login.css";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = require("../../data/en/cms/login");
        this.changeLang = this.changeLang.bind(this);
    }

    componentWillReceiveProps(props) {
        this.getProps(props);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.hover);
        this.getProps(this.props);
    }

    getProps(props) {
        if (props.lang) {
            this.setState(require(`../../data/${props.lang}/cms/login`));
        }
    }

    changeLang() {
        this.props.changeLang(this.state.lang === "en" ?  "pl" : "en");
    }

    render() {
        let langClass = this.state.lang === "en" ?  "lang pl" : "lang en";
        return (
            <div id="login">
                <div className="container">
                    <div id="languageSelect">
                        <div className={langClass} onClick={() => this.changeLang()}/>
                    </div>
                </div>
            </div>
        );
    }
}
