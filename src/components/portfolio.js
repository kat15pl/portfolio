import React from "react";
import Portfoliosection from "./portfolioSection";
import Section from "./Section";
import "./../css/portfolio.css";

export default class Portfolio extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: "portfolio",
            header: "Portfolio",
            lang: props.lang,
            content: this.getPortfolioSection(props)
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            lang: props.lang,
            content: this.getPortfolioSection(props)
        })
    }

    getPortfolioSection(props) {
        return require("../data/portfolio").map((portfolio, index) => {
            let src = require(`./../img/portfolio/${portfolio.img}`),
                delay = `${index * 500}ms`;
            return (
                <div className="col-sm-3 portfolio-item" key={index}>
                    <div>
                        <Portfoliosection delay={delay}
                                          id={portfolio.id}
                                          img={src}
                                          lang={props.lang}
                                          tags={portfolio.tags}
                                          url={portfolio.url}/>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <Section id={this.state.id} header={this.state.header} content={this.state.content} isVisible={false}/>
        );
    }
}