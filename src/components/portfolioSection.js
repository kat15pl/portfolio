import React from "react";
import PropTypes from 'prop-types';

export default class Portfoliosection extends React.Component {

    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            lang: props.lang,
            id: props.id,
            img: props.img,
            delay: props.delay,
            portfolio: require(`../data/${props.lang}/modal`),
            tags: props.tags
        };
        this.openPortfolio = this.openPortfolio.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            lang: props.lang,
            id: props.id,
            img: props.img,
            delay: props.delay,
            portfolio: require(`../data/${props.lang}/modal`),
            tags: props.tags
        });
    }

    openPortfolio() {
        this.context.router.history.push(`/portfolio/${this.state.lang}/${this.state.id}`);
    }

    render() {
        return (
            <div className="portfolio-link animated bounceInRight" style={{animationDelay: `${this.state.delay}`}}
                 onClick={this.openPortfolio}>
                <div className="front" style={{backgroundImage: `url(${this.state.img})`}}/>
                <div className="back">
                    <div className="inner">
                        <p>
                            {this.state.portfolio.technologies}:
                        </p>
                        {this.state.tags.map((tag, index) => {
                                return (<span key={index}>
                                            {tag}
                                        </span>
                                )
                            }
                        )}
                    </div>
                </div>
            </div>
        );
    }
}