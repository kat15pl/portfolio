import React from "react";
import "./../css/scrollTop.css";

export default class ScrollTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMe: false,
            hover: false
        };
        this.handleScroll = this.handleScroll.bind(this);
        this.scrollToTop = this.scrollToTop.bind(this);
        this.hover = this.hover.bind(this);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    scrollToTop() {
        this.setState({
            scrollY: window.scrollY,
            scrollToTopAnimation: setInterval(this.scrollAnimation.bind(this), 25)
        })
    }

    scrollToTopAnimation() {
        let scrollY = this.state.scrollY - 100;
        this.setState({scrollY: scrollY});
        window.scrollTo(0, scrollY);
        if (scrollY <= 0) {
            clearInterval(this.state.scrollAnimation);
        }
    }

    handleScroll(event) {
        let showMe = false;
        if (window.scrollY > 100) {
            showMe = true;
        }
        this.setState({
            showMe: showMe
        });
    }

    hover(event) {
        this.setState({hover: !this.state.hover});
    }

    render() {
        let scrollTopClass = `scroll-top page-scroll animated ${this.state.showMe ? (this.state.hover ? 'bounce' : ''): "hidden"}`;
        return (
            <div className={scrollTopClass} onMouseEnter={this.hover} onMouseLeave={this.hover}>
                <a className="btn btn-primary" onClick={this.scrollToTop}>
                    <i className="fa fa-chevron-up"></i>
                </a>
            </div>
        );
    }
}
