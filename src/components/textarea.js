import React from "react";

export default class Textarea extends React.Component {

    constructor(props) {
        super(props);
        this.state = ({
            label: props.label,
            placeholder: props.placeholder,
            name: props.name,
            id: props.id,
            isChanged: false,
            validationError: props.errorLabel,
            error: props.error
        });

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            label: props.label,
            placeholder: props.placeholder,
            name: props.name,
            id: props.id,
            validationError: props.errorLabel,
            error: props.error
        });
    }

    handleChange(event) {
        let isChanged;
        isChanged = event.target.value !== "";
        this.setState({
            isChanged: isChanged
        });
        this.props.onChange(this.state.id, event.target.value);
    }

    render() {
        let classes = [
            "form-group",
            "col-xs-12",
            "floating-label-form-group",
            "controls"
        ];

        if (this.state.isChanged) {
            classes.push("floating-label-form-group-with-value");
        }

        return (
            <div className={classes.join(" ")}>
                <label>{this.state.label}:</label>
                {this.state.error ? (
                    <p className="help-block text-danger">{this.state.validationError}</p>
                ) : (<p></p>)}
                <textarea type="text" className="form-control" placeholder={this.state.placeholder} id={this.state.id}
                          name={this.state.name} onChange={this.handleChange}/>
            </div>
        );
    }
};