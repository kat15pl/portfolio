import React, {Component} from "react";
import ReactDOM from "react-dom";
import "./../css/animateOnVisible.css";

export default class Spy extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            offset: 0,
            animateIn: "",
            delay: "",
            duration: "",
            maxWidth: "",
            once: false
        };
        this.handleScroll = this.handleScroll.bind(this);
        this.getProps = this.getProps.bind(this);
    }

    componentWillReceiveProps(props) {
        this.getProps(props);
    }

    componentDidMount() {
        this.handleScroll();
        window.addEventListener("scroll", this.handleScroll);
        this.getProps(this.props);
    }

    getProps(props) {
        this.setState({
            animateIn: props.animateIn || "",
            delay: props.delay || "",
            duration: props.duration || "",
            once: props.once !== undefined ? props.once : false,
            offset: props.offset || 0
        });
        if (props.animateIn === "typing") {
            let width = ReactDOM.findDOMNode(this.component).offsetWidth;
            this.setState({
                maxWidth: `${width}px`
            });
        }
    }

    handleScroll(event) {
        if (!ReactDOM.findDOMNode(this.component)) {
            return;
        }
        let menuHeight = 100,
            top = ReactDOM.findDOMNode(this.component).getBoundingClientRect().top,
            bottom = ReactDOM.findDOMNode(this.component).getBoundingClientRect().bottom,
            isVisible = (top + this.state.offset <= window.innerHeight - menuHeight) && (bottom >= 0 && bottom + this.state.offset <= window.innerHeight - menuHeight);
        if (this.state.once && !this.state.isVisible) {
            this.setState({
                isVisible: isVisible
            });
        } else if (!this.state.once) {
            this.setState({
                isVisible: isVisible
            });
        }
    }

    render() {
        let className = `animated ${this.state.isVisible ? this.state.animateIn : ''}`;
        return (
        <div className={className}
             ref={(component) => {
                 this.component = component
             }}
             style={{
                 animationDelay: `${this.state.delay}`,
                 animationDuration: `${this.state.duration}`,
                 maxWidth: `${this.state.maxWidth}`
             }}>
            {this.props.children}
        </div>);
    }
}
