import React from "react";
import Sectiontwocolumns from "./SectionTwoColumns";
import "./../css/about.css";

export default class About extends React.Component {

    constructor(props) {
        super(props);
        this.state = require(`../data/${props.lang}/about`);
    }

    componentWillReceiveProps(props) {
        if (props.lang) {
            this.setState(require(`../data/${props.lang}/about`));
        }
    }

    render() {
        return (
            <Sectiontwocolumns id={this.state.id} header={this.state.header}
                               leftColumn={this.state.left} rightColumn={this.state.right}/>
        );
    }
}
