import React from "react";
import Scrollspy from "react-scrollspy";
import "./../css/menu.css";

export default class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            affixClass: "affix-top",
            navbarClickedClass: "",
            lang: props.lang,
            menu: require(`../data/${props.lang}/menu`),
            mobileMenuHeight: 0
        };
        this.handleScroll = this.handleScroll.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.changeLang = this.changeLang.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.lang) {
            this.setState({
                lang: props.lang,
                menu: require(`../data/${props.lang}/menu`)
            });
        }
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    toggleMenu() {
        clearInterval(this.state.menuAnimationExpand);
        clearInterval(this.state.menuAnimationCollapse);
        if (this.state.mobileMenuHeight > 0) {
            this.setState({
                navbarClickedClass: "",
                menuAnimationCollapse: setInterval(this.menuCollapseAnimation.bind(this), 25)
            })
        } else {
            this.setState({
                navbarClickedClass: "opened",
                menuAnimationExpand: setInterval(this.menuExpandAnimation.bind(this), 25)
            })
        }
    }

    menuExpandAnimation() {
        this.setState({
            "prevHeight": this.menuCollapse.offsetHeight
        });
        let mobileMenuHeight = this.state.mobileMenuHeight + 10;
        this.setState({mobileMenuHeight: mobileMenuHeight});
        if (this.state.prevHeight === this.menuCollapse.offsetHeight) {
            clearInterval(this.state.menuAnimationExpand);
        }
    }

    menuCollapseAnimation() {
        let mobileMenuHeight = this.state.mobileMenuHeight - 10;
        this.setState({mobileMenuHeight: mobileMenuHeight});
        if (mobileMenuHeight <= 0) {
            clearInterval(this.state.menuAnimationCollapse);
        }
    }

    handleScroll(event) {
        let affixClass = "affix";
        if (window.scrollY < 100) {
            affixClass = "affix-top";
        }
        this.setState({
            affixClass: affixClass
        });
    }

    changeLang() {
        this.props.changeLang(this.state.lang === "en" ? "pl" : "en");
    }

    render() {
        let navClass = `navbar navbar-default navbar-fixed-top navbar-custom ${this.state.affixClass}`,
            navbarToggleClass = `navbar-toggle ${this.state.navbarClickedClass}`,
            menuClass = `navbar-collapse collapse${this.state.mobileMenuHeight === 0 ? "" : " show"}`,
            list = ["header"],
            langClass = this.state.lang === "en" ? "lang pl" : "lang en";
        list = list.concat(this.state.menu.map(function (value) {
            return value.id;
        }));
        return (
            <div id="mainNav" className={navClass}>
                <div className="container">
                    <div className="navbar-header page-scroll">
                        <button type="button" className={navbarToggleClass} onClick={this.toggleMenu}>
                            <span className="sr-only">Toggle navigation</span> Menu
                            <span className="menu-toggle">
                                <i/>
                                <i/>
                                <i/>
                            </span>
                        </button>
                    </div>

                    <div className={menuClass} id="menu-collapse"
                         style={{height: "auto", maxHeight: this.state.mobileMenuHeight}}
                         ref={(component) => {
                             this.menuCollapse = component;
                         }}>
                        <Scrollspy items={list} className="nav navbar-nav navbar-right"
                                   currentClassName="active">
                            <li className="page-scroll hidden">
                                <a href="#header">Empty</a>
                            </li>
                            {this.state.menu.map((item, index) => {
                                let link = `#${item.id}`;
                                return (
                                    <li className="page-scroll" key={index}>
                                        <a href={link}>
                                            <span className="text">{item.name}</span>
                                            <span className="line -right"></span>
                                            <span className="line -top"></span>
                                            <span className="line -left"></span>
                                            <span className="line -bottom"></span>
                                        </a>
                                    </li>
                                );
                            })}
                        </Scrollspy>
                    </div>
                    <div id="languageSelect">
                        <div className={langClass} onClick={() => this.changeLang()}/>
                    </div>
                </div>
            </div>
        );
    }
}
