import React from "react";
import AnimateOnVisible from "./animateOnVisible";
import "./../css/section.css";

export default class Section extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            header: props.header,
            content: props.content,
            className: props.className || ""
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            id: props.id,
            header: props.header,
            content: props.content,
            className: props.className || ""
        });
    }

    render() {
        return (
            <section id={this.state.id}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h2>
                                {this.state.header.split("").map((letter, index) => {
                                    let delay = `${index * 500}ms`;
                                    return (
                                        <AnimateOnVisible animateIn="fadeIn" delay={delay} key={index} once={true}>
                                            {letter}
                                        </AnimateOnVisible>
                                    )
                                })}
                            </h2>
                            <hr className="star-primary"/>
                        </div>
                    </div>
                    <div className="row">
                        {this.state.content}
                    </div>
                </div>
            </section>
        );
    }
}
