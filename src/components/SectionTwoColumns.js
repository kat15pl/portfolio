import React from "react";
import AnimateOnVisible from "./animateOnVisible";
import Section from "./Section";

export default class Sectiontwocolumns extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            header: props.header,
            content: this.getContent(props)
        };
    }

    getContent(props) {
        return (<div>
            <div className="col-sm-4 col-sm-offset-2">
                <AnimateOnVisible animateIn="bounceInRight" once={true} duration={"2.5s"}>
                    <p>{props.leftColumn}</p>
                </AnimateOnVisible>
            </div>
            <div className="col-sm-4">
                <AnimateOnVisible animateIn="bounceInLeft" once={true} duration={"2.5s"}>
                    <p>{props.rightColumn}</p>
                </AnimateOnVisible>
            </div>
        </div>);
    }

    componentWillReceiveProps(props) {
        this.setState({
            id: props.id,
            header: props.header,
            content: this.getContent(props)
        });
    }

    render() {
        return (
            <Section id={this.state.id} header={this.state.header} content={this.state.content} isVisible={false}/>
        );
    }
}
