import React from "react";
import Modal from "react-modal";
import "./../css/modals.css";

export default class Portfoliomodals extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: props.modalIsOpen,
            modalContent: props.modalContent,
            modalTags: props.modalTags || [],
            modalUrl: props.modalUrl,
            modal: require(`../data/${props.lang}/modal`)
        };

        this.closeModal = this.closeModal.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            modalIsOpen: props.modalIsOpen,
            modalContent: props.modalContent,
            modalTags: props.modalTags || [],
            modalUrl: props.modalUrl,
            modal: require(`../data/${props.lang}/modal`)
        });
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        this.props.closeModal();
    }

    render() {
        return (
            <Modal isOpen={this.state.modalIsOpen}
                   ariaHideApp={false}
                   onRequestClose={this.closeModal}
                   style={{
                       content: {
                           top: "0",
                           left: "0",
                           right: "auto",
                           bottom: "auto",
                           width: "100%",
                           height: "100%"
                       }
                   }}
                   className="animated zoomIn"
                   contentLabel="Modal">
                <div className="modal-content">
                    <div className="close-modal" onClick={this.closeModal}>
                        <div className="lr">
                            <div className="rl">
                            </div>
                        </div>
                    </div>
                    <div className="modal-body">
                        <h2>{this.state.modalContent.header}</h2>
                        <div className="description">{this.state.modalContent.description}</div>
                        <a href={this.state.modalUrl} className="website"><span
                            className="fa fa-external-link"></span>{this.state.modal.url}</a>
                        <h3 className="header">{this.state.modal.technologies}:</h3>
                        <ul>
                            {this.state.modalTags.map((tag, index) => {
                                return (
                                    <li key={index}>
                                        {tag}
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </Modal>
        );
    }
}
