import React, {Component} from "react";
import "./../../css/browser.css";

export default class Browser extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            image: this.props.image,
            scrollY: 0
        };
        this.hover = this.hover.bind(this);
        this.mouseOut = this.mouseOut.bind(this);
    }

    hover(event) {
        this.setState({
            maxHeight: this.image.offsetHeight - this.container.offsetHeight + this.statusBar.offsetHeight + this.tabBar.offsetHeight,
            scrollAnimation: setInterval(this.scrollAnimation.bind(this), 25)
        });
    }

    scrollAnimation() {
        let scrollY = this.state.scrollY - 10;
        this.setState({scrollY: scrollY});
        // console.log(scrollY +":"+ (-this.state.maxHeight));
        if (scrollY <= -this.state.maxHeight) {
            this.setState({scrollY: -this.state.maxHeight});
            clearInterval(this.state.scrollAnimation);
        }
    }

    mouseOut() {
        clearInterval(this.state.scrollAnimation);
    }

    render() {
        let image = require(`./../../img/portfolio/${this.state.image}`);
        return (
            <div className="browser" onMouseOver={this.hover} onMouseOut={this.mouseOut}
                 ref={(component) => {
                     this.container = component;
                 }}>
                <div className="status-bar"
                     ref={(component) => {
                         this.statusBar = component;
                     }}>
                    <div className="buttons">
                    </div>
                </div>
                <div className="tab-bar"
                     ref={(component) => {
                         this.tabBar = component;
                     }}>
                </div>
                <div className="window">
                    <img className="scroll-img" src={image}
                         style={{top: `${this.state.scrollY}px`}} alt=""
                         ref={(component) => {
                             this.image = component;
                         }}/>
                </div>
            </div>
        );
    }
}
