import React from "react";
import profile from "../img/profile.png";
import AnimateOnVisible from "./animateOnVisible";
import "./../css/header.css";

export default class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            header: require(`../data/${props.lang}/header`)
        }
    }

    componentWillReceiveProps(props) {
        if (props.lang) {
            this.setState({
                header: require(`../data/${props.lang}/header`)
            })
        }
    }

    render() {
        return (
            <header className="App-header" id="header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <AnimateOnVisible animateIn="bounceInLeft" duration="1.5s" once={true}>
                                <img className="img-responsive gray" src={profile} alt=""/>
                            </AnimateOnVisible>
                            <div className="intro-text">
                                <AnimateOnVisible animateIn="bounceInRight" duration="1s" delay="1.5s" once={true}>
                                    <h1 className="name">{this.state.header.header}</h1>
                                </AnimateOnVisible>
                                <hr className="star-light"/>
                                <AnimateOnVisible animateIn="typing" once={true} delay="3s">
                                    <span className="skills">{this.state.header.skills}</span>
                                </AnimateOnVisible>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}