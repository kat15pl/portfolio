import React, {Component} from "react";
import PropTypes from 'prop-types';
import Browser from "./../components/portfolioSite/Browser";
import "./../css/portfolioSite.css";

export default class PortfolioSite extends Component {

    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props, context) {
        super(props, context);
        let params = this.props.match.params,
            portfolio = null,
            portfolioFile = require(`./../data/${params.siteLang}/portfolio`);
        require("./../data/portfolio").map((entry, index) => {
            if (entry.id === params.site) {
                portfolio = entry;
            }
            return null;
        });
        if (!portfolioFile[params.site] || !portfolio) {
            this.context.router.history.push(`/`);
        }
        this.state = {
            siteLang: params.siteLang,
            portfolio: portfolio,
            data: portfolioFile[params.site],
            text: require(`./../data/${params.siteLang}/modal`)
        };
        this.changeLang = this.changeLang.bind(this);
        this.closePortfolio = this.closePortfolio.bind(this);
    }

    changeLang(lang) {
        this.setState({
            siteLang: lang
        })
    }

    closePortfolio() {
        this.context.router.history.push(`/`);
    }

    render() {
        return (
            <div id="portfolioSite">
                <div className="close-modal" onClick={this.closePortfolio}>
                    <div className="lr">
                        <div className="rl">
                        </div>
                    </div>
                </div>
                <div className="left">
                    <div className="title">
                        <h2>
                            {this.state.data.header}
                        </h2>
                        <div className="type">
                            {this.state.data.content}
                        </div>
                    </div>
                    <div className="description">
                        <h3 className="header">
                            {this.state.text.description}:
                        </h3>
                        <div className="content">
                            {this.state.data.description}
                        </div>
                    </div>
                    <div className="tags">
                        <h3 className="header">
                            {this.state.text.technologies}:
                        </h3>
                        <div className="content">
                            {this.state.text.technologiesDescription}
                            <ul>
                                {this.state.data.tagsLong.map((tag, index) => {
                                        return (<li key={index}>
                                            {tag}
                                        </li>)
                                    }
                                )}
                            </ul>
                        </div>
                    </div>
                    <div className="url">
                        <a href={this.state.portfolio.url} className="btn btn-success btn-lg">
                            {this.state.text.url}
                        </a>
                    </div>
                </div>
                <div className="right">
                    <div className="desktop">
                        {this.state.portfolio.desktop ?
                            (
                                <Browser image={this.state.portfolio.desktop}/>
                            ) : null
                        }
                    </div>
                    <div className="tablet">
                        {this.state.portfolio.tablet ?
                            (
                                <Browser image={this.state.portfolio.tablet}/>
                            ) : null
                        }
                    </div>
                    <div className="mobile">
                        {this.state.portfolio.mobile ?
                            (
                                <Browser image={this.state.portfolio.mobile}/>
                            ) : null
                        }
                    </div>
                </div>
            </div>
        );
    }
}
