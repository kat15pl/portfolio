import React, {Component} from "react";
import Login from "../components/cms/login";
import Footer from "../components/cms/footer";

export default class CmsSite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            siteLang: "en"
        };
        this.changeLang = this.changeLang.bind(this);
    }

    changeLang(lang) {
        this.setState({
            siteLang: lang
        })
    }

    render() {
        return (
            <div id="cms">
                <Login lang={this.state.siteLang} changeLang={this.changeLang}/>
                <Footer lang={this.state.siteLang}/>
            </div>
        );
    }
}
