import React, {Component} from "react";
import Menu from "../components/menu";
import Header from "../components/header";
import Portfolio from "../components/portfolio";
import About from "../components/about";
import Contact from "../components/contact";
import ScrollTop from "../components/scrollTop";
import Footer from "../components/footer";
import "animate.css/animate.min.css";

export default class MainSite extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            siteLang: "en"
        };
        this.changeLang = this.changeLang.bind(this);
    }

    changeLang(lang) {
        this.setState({
            siteLang: lang
        })
    }

    render() {
        return (
            <div id="site">
                <Menu lang={this.state.siteLang} changeLang={this.changeLang}/>
                <Header lang={this.state.siteLang} isVisible={false}/>
                <Portfolio lang={this.state.siteLang} isVisible={false}/>
                <About lang={this.state.siteLang} isVisible={false}/>
                <Footer lang={this.state.siteLang}/>
                <ScrollTop lang={this.state.siteLang}/>
            </div>
        );
    }
}
